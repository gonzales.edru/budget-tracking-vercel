import Head from 'next/head'
import { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Form, Row, Col, Button, Modal, Card } from 'react-bootstrap'

export default function Records() {
	const { user, setUser } = useContext(UserContext)
	const [show, setShow] = useState(false);
	const [description, setDescription] = useState('')
	const [category, setCategory] = useState(0)
	const [sortCategory, setSortCategory] = useState('0')
	const [categories, setCategories] = useState([])
	const [name, setName] = useState(0)
	const [amount, setAmount] = useState(0)
	const [isActive, setIsActive] = useState(false)
	const [transactions, setTransactions] = useState([])
	const [search, setSearch] = useState('')

	const handleClose = () => {
		setShow(false)
		setCategory(0)
		setAmount(0)
		setDescription('')
		setName(0)
	}
	
	const handleShow = () => setShow(true);

	const categoryName = categories.map(country => {
		return(
			<option value={country.name}>{country.name}</option>
		)
	})

	const transactionData = transactions.map(transaction => {
		return(
			<Card
			    bg="light"
			    text="dark"
			    key={transaction._id}
			    style={{ width: '18rem' }}
			    className="mb-2 w-100"
			  >
			    <Card.Header>{transaction.description}</Card.Header>
			    <Card.Body>
				    <Card.Title>
				        <Row>
				    		<Col sm={6} className="d-flex">
				    			{transaction.name}
				    	   	</Col>
				    	   
				    	    { transaction.category === 'income'
				    	    	?
				    	    	<Col sm={6} className="d-flex justify-content-end">	
				    	    		+ {transaction.amount}
				    	    	</Col>	
				    			:
				    			<Col sm={6} className="d-flex justify-content-end">	
				    				- {transaction.amount}
				    			</Col>	
				    	    }
				    	    
				    	</Row>
				    </Card.Title>
			      	<Row>
					    <Col sm={6} className="d-flex">
							{transaction.Date.slice(0,10)}
					    </Col>
					    <Col sm={6} className="d-flex justify-content-end">
							{transaction.income}
					    </Col>
				  	</Row>
				  	<Card.Text>
				  		{transaction.category}
				  	</Card.Text>
			    </Card.Body>
			</Card>  
		)
	})

	useEffect(() => {
		if (amount < 0) {
			setAmount(0)
		}
	})



	useEffect(() => {
		if(category == '')
		{
			setIsActive(true)	
		}
		else
		{
			const accessToken  = localStorage.getItem('token')
			const options = {
	          	method: 'POST',
	          	headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`  },
	          	body: JSON.stringify({ category: category, id: user.id  })
			}
			      
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/`, options).then(res => res.json()).then(data => {
	          	if(data.auth == 'failed'){
	          		window.location.replace('/accounts/login')
	          	}
	          	else
	          	{
	          		setCategories(data)
	          		setIsActive(false)
	          	}	
	      	})
		}
		
        
	},[category])

	useEffect(() => {
		
		const accessToken  = localStorage.getItem('token')
		const options = {
          	method: 'GET',
          	headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`   }
		}
		      
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/records`, options).then(res => res.json()).then(data => {
          	if(data.auth == 'failed'){
          		window.location.replace('/accounts/login')	
          	}
          	else
          	{
          		if(sortCategory !== '0')
          		{
          			const trans = data.transactions.filter(transaction => transaction.category === sortCategory)
					setTransactions(trans)	
          		}
          		else if (search !== '') 
          		{
          			const trans = data.transactions.filter(transaction => transaction.name.toLowerCase().includes(search.toLowerCase()))
          			setTransactions(trans)
          		}
          		else
          		{
      				setTransactions(data.transactions)		
          		}
          		
          	}	
      	})
	}, [sortCategory, search])

	function enroll(e){

		e.preventDefault()

		if(name === '' || description === ''){
			alert("Please fill up all the required fields")
		}
		else
		{
			const accessToken  = localStorage.getItem('token')
			const options = {
	          	method: 'POST',
	          	headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`  },
	          	body: JSON.stringify({ id: user.id, amount: amount, category: category, description:description, name:name})
			}
			      
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/enrollRecord`, options).then(res => res.json()).then(data => {
	          	if(data !== null){
	          		setTransactions(data.transactions)
	          		handleClose()
	          		setCategory(0)
	          		setAmount(0)
	          		setDescription('')
	          		setName(0)
	          	}
	          	else
	          	{
	          		alert("Something went wrong")
	          	}	
	      	})
		}
	}

	return(
			<React.Fragment>
		   		<Head>
		   		  <title>Records</title>
		   		  <link rel="icon" href="/favicon.ico" />
		   		</Head>
		   		{ user.id !== undefined
		   			? 
		   			<React.Fragment> 	
			   			<div className="mt-5 pt-5">
				        	<h4 className="text-center">Records</h4>
					    </div>
					    <Form className="container">
						    <Form.Row className="align-items-center">
						    	<Col sm={12} lg={4}>
						    	  <Button className="record-button-width text-center mb-2 bg-dark" action onClick = {handleShow}>
						    	    Add record
						    	  </Button>
						    	</Col>
						       	<Col sm={4}>
							        <Form.Control
								        className="mb-2"
								        id="inlineFormInput"
								        placeholder="Search"
								        value = {search} onChange = {(e) => setSearch(e.target.value)}
							        />
						        </Col>
						        <Col sm={4}>
							       	<Form.Control as="select" className="mb-2" defaultValue={sortCategory} onChange = {(e) => setSortCategory(e.target.value)}>
								       	    <option value="0" >Transactions</option>
								       	    <option value="income">Income</option>
								       	    <option value="expense">Expense</option>
							       	</Form.Control>
						       	</Col>
						    </Form.Row>
						    <Col sm={12}>
								{transactionData}
						    </Col>
					    </Form>
					   
					</React.Fragment> 
				    :
				    	window.location.replace('/accounts/login')
				    } 



		    	    <Modal
		    	    	show={show} onHide={handleClose}
		    	    	size="lg"
		    	    	key={transactions._id}
		    	    	aria-labelledby="contained-modal-title-vcenter"
		    	    	centered
		    	    	>
		    	    	<Modal.Header closeButton>
		    	        	<Modal.Title id="contained-modal-title-vcenter">
		    	          		New Record
		    	        	</Modal.Title>
		    	      	</Modal.Header>
		    	      	<Modal.Body>
		      		        <Form.Group as={Row} controlId="formPlaintextEmail">
        	                  	<Form.Label column sm="3">
        		                    Category
        		                </Form.Label>
        		                <Col sm="9">
        		                    <Form.Control as="select" className="mb-2" defaultValue={category} onChange={(e) => setCategory(e.target.value)} required>
        					       	    <option value='0' disabled>Choose...</option>
        					       	    <option value="income">Income</option>
        					       	    <option value="expense">Expense</option>
        				       		</Form.Control>
        		                </Col>
        	                </Form.Group>
        	  		        <Form.Group as={Row} controlId="formPlaintextEmail">
        	                  	<Form.Label column sm="3">
        		                    Name
        		                </Form.Label>
        		                <Col sm="9">
        		                    <Form.Control as="select" className="mb-2" defaultValue={name} onChange={(e) => setName(e.target.value)} disabled={isActive} required>
        					       	    <option value="0" disabled>Choose...</option>
        					       	    {categoryName}
        				       		</Form.Control>
        		                </Col>
        	                </Form.Group>
        	  		        <Form.Group as={Row} controlId="formPlaintextEmail">
        	                  	<Form.Label column sm="3">
        		                    Description
        		                </Form.Label>
        		                <Col sm="9">
        		                    <Form.Control type="text" className="mb-2" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} disabled={isActive} required />
        		                </Col>
        	                </Form.Group>
              		        <Form.Group as={Row} controlId="formPlaintextEmail">
                              	<Form.Label column sm="3">
            	                    Amount
            	                </Form.Label>
            	                <Col sm="9">
            	                    <Form.Control type="number" disabled={isActive} value={amount} onChange={(e) => setAmount(e.target.value)}  required />
            	                </Col>
                            </Form.Group>
		    	      	</Modal.Body>
		    	      	<Modal.Footer className="d-flex justify-content-center">
		    	      		<Button variant="primary" className="modal-button-width" onClick={enroll}>Submit</Button>
		    	      		<Button className="modal-button-width" onClick={handleClose}>Close</Button>
		    	      	</Modal.Footer>
		    	    </Modal>
		  	</React.Fragment>

		  	      


	)
}