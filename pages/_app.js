import '../styles.css'
import { useState, useEffect } from 'react';
import { UserProvider } from '../UserContext';
import { useBeforeunload } from 'react-beforeunload';
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import moment from 'moment'
import IdleTimerContainer from '../components/idleTimerContainer'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/vendor/icofont/icofont.min.css'
function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id: null
	})

	useEffect(() => {
		const accessToken  = localStorage.getItem('token')

		const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/details`, options).then(res => res.json()).then(data => {
            setUser({ id: data._id})
        })
        
	}, [user.firstName])

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null
		})		
	}

  	return(
  		<UserProvider value={{user, setUser, unsetUser }}>
  		<IdleTimerContainer />
  			<React.Fragment>
	  			<NavBar />
				<Component {...pageProps} />
				<Footer />
			</React.Fragment>
		</UserProvider>
  	) 
}

export default MyApp
