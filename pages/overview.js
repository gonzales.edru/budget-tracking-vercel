import { Line } from "react-chartjs-2";
import { Form, Row, Col, Card, Button } from "react-bootstrap"
import { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Overview(){
	const { user, setUser } = useContext(UserContext)
	const [fromDate, setFromDate] = useState('');
	const [toDate, setToDate] = useState('')
	const [isActive, setIsActive] = useState(true)
	const [validation, setValidation] = useState(false)
	const [transaction, setTransaction] = useState([])
	const [balance, setBalance] = useState('')

	useEffect(() => {
		const accessToken  = localStorage.getItem('token')
		const options = {
          	method: 'GET',
          	headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`   }
		}
		      
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/records`, options).then(res => res.json()).then(data => {
          	if(data.auth == 'failed'){
          		window.location.replace('/accounts/login')	
          	}
          	else
          	{
          		const trans = data.transactions.filter(transaction => transaction.Date.slice(0,10) <= toDate && transaction.Date.slice(0,10) >= fromDate )
          		const Expense = 0
          		setTransaction(trans)
          		setBalance(data.totalIncome)
          	}	
      	})
	}, [toDate])

	useEffect(() => {
		if (fromDate !== '') {
			setIsActive(false)
			setToDate('')
		}
		else{
			setIsActive(true)
			setToDate('')
		}	
	}, [fromDate])

	useEffect(() => {

		if(toDate !== ''){
			if (transaction.length <= 0){
				alert("No Records Found")
				setValidation(false)
			}
			else
			{
				setValidation(true)
			}	
		}
		else
		{
			setValidation(false)
		}
		
	}, [transaction])

	const transactionDate = transaction.map(transaction => {
		return 	transaction.Date.slice(0,10)
	})
	const transactionIncome = transaction.map(transaction => {
		return 	transaction.income
	})
	const transactionExpense = transaction.map(transaction => {
		return 	transaction.expense
	})

	const Expenses = transaction.filter(transaction => transaction.category == "expense")
	const Income = transaction.filter(transaction => transaction.category == "income")

	let totalExpenses
	let totalIncome

	if(transaction.length > 0){
		totalExpenses = Expenses.reduce((total, expense) => +total + +expense.amount, 0)
		totalIncome = Income.reduce((total, expense) => +total + +expense.amount, 0)
	}	
	
	const transactionData = transaction.map(transaction => {
		return(
			<Card
			    bg="light"
			    text="dark"
			    key={transaction._id}
			    style={{ width: '18rem' }}
			    className="mb-2 w-100"
			  >
			    <Card.Header>{transaction.description}</Card.Header>
			    <Card.Body>
				    <Card.Title>
				        <Row>
				    		<Col sm={6} className="d-flex">
				    			{transaction.name}
				    	   	</Col>
				    	</Row>
				    </Card.Title>
			      	<Row>
					    <Col sm={6} className="d-flex">
							{transaction.Date.slice(0,10)}
					    </Col>
					    <Col sm={6} className="d-flex justify-content-end">
							{transaction.income}
					    </Col>
				  	</Row>
				  	<Card.Text>
				  		{transaction.category}
				  	</Card.Text>
			    </Card.Body>
			</Card>  
		)
	})
	const data = {
		labels: transactionDate,
		datasets: [
		    {
		      label: "Income",
		      data: transactionIncome,
		      fill: true,
		      backgroundColor: "rgba(75,192,192,0.2)",
		      borderColor: "rgba(75,192,192,1)"
		    },
		    {
		      label: "Expense",
		      data: transactionExpense,
		      fill: true,
		      backgroundColor: "rgba(75,192,192,0.2)",
		      borderColor: "#742774"
		    }

		]
	};
	return (
	    <div className="container">
	    	<Row className="justify-content-center mb-3 mt-3">
	    	<h3>Overview</h3>
	    	</Row>
			<Form>
			    <Form.Group as={Row} controlId="formPlaintextEmail">
			       	<Form.Label column sm="2">
			         	Date From: 
				    </Form.Label>
			       	<Col sm="4">
			         	<Form.Control type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value)} />
			       	</Col>
		       		<Form.Label column sm="2">
		       	 		Date To:
		       		</Form.Label>
		       		<Col sm="4">
		       	  		<Form.Control type="date" disabled={isActive} value={toDate} onChange={(e) => setToDate(e.target.value)} />
		       		</Col>
			     </Form.Group>

			</Form>
			{ validation === true 
				? 	<React.Fragment>
						<Line data={data} />
						<Row>
							<Col sm={4} xs={4} className = "mt-3">
								<Card className="text-center">
								    <Card.Text>
								      	Expense
								    	&#8369;	{totalExpenses}
								    </Card.Text>
								</Card>
							</Col>
							<Col sm={4} xs={4} className = "mt-3">
								<Card className="text-center">
								    <Card.Text>
								      	Balance
								    	&#8369;	{balance}
								    </Card.Text>
								</Card>
							</Col>
							<Col sm={4} xs={4} className = "mt-3">
								<Card className="text-center">
								    <Card.Text>
								      	Income
								    	&#8369; {totalIncome}	
								    </Card.Text>
								</Card>
							</Col>
						</Row>
						<Row>
							<Col sm={12} className="mt-3">
								{transactionData}
							</Col>
						</Row>
					</React.Fragment>
				: 

				null
			}
	      	
	    </div>
	);	
}
