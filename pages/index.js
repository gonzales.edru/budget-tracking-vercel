import Head from 'next/head'
import styles from '../styles/Home.module.css'
import NavBar from '../components/NavBar'
import { Button, Container, Row, Col } from 'react-bootstrap'

export default function Home() {
  return (
    <React.Fragment>
      <section id="hero" className="d-flex flex-column justify-content-center align-items-center">
          <div className="container-main text-center text-md-left" data-aos="fade-up">
            <h1>Welcome to R.I.C.E Budget Tracker</h1>
            <h2>This web app will allow you to track your income and expense so you can plan your finances</h2>
            <a href="/records" className="btn-get-started scrollto">Get Started</a>
          </div>
      </section>
      
    </React.Fragment>
  )
}
