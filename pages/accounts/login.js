import Head from 'next/head'
import { useState, useContext, useEffect } from 'react';
import {Col} from 'react-bootstrap'
import UserContext from '../../UserContext';
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import Router from 'next/router'
import moment from 'moment'
import Container from '@material-ui/core/Container';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';

export default function Home() {
  const [tokenId, setTokenId] = useState(null)
  const { user, setUser } = useContext(UserContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [checked, setChecked] = useState(false)

  const authenticate = (e) => {
      e.preventDefault()

      const options = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json'  },
          body: JSON.stringify({ email: email, password: password })
      }
      
      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/loginManually`, options).then(res => res.json()).then(data => {
          if (typeof data.accessToken !== 'undefined') {
              localStorage.setItem('token', data.accessToken)
              retrieveUserDetails(data.accessToken)
          } else {
              if (data.error === 'does-not-exist') {
                  Swal.fire('Authentication Failed', 'User does not exist.', 'error')
              } else if (data.error === 'incorrect-password') {
                  Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
              } else if (data.error === 'login-type-error') {
                  Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
              }
          }
      })
  }

  const retrieveUserDetails = (accessToken) => {
      const options = {
          headers: { Authorization: `Bearer ${ accessToken }` } 
      }
      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/details`, options).then(res => res.json()).then(data => {
          setUser({ id: data._id})
          Router.push('/records')
      })
      
  }

  const authenticateGoogleToken = (response) => {
      console.log(response)
      setTokenId(response.tokenId)
      const payload = {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ tokenId: response.tokenId })
      }

      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/login`, payload).then(res => res.json()).then(data => {
          if (typeof data.accessToken !== 'undefined') {
              localStorage.setItem('token', data.accessToken)
              retrieveUserDetails(data.accessToken)
          } else {
              if (data.error === 'google-auth-error') {
                  Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
              } else if (data.error === 'login-type-error') {
                  Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
              }
          }
      })
  }

  const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(10),
      marginBottom: theme.spacing(41.3),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(1, 0, 2),
    },

  }));
  const classes = useStyles();
  
  return (
    <React.Fragment>
      <Head>
          <title>Log in</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      { user.id == undefined 
        ?
        <React.Fragment>  
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5" className="text-center">
                Sign in
              </Typography>
              <form className={classes.form} onSubmit={(e) => {authenticate(e)}} validate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  id="email"
                  name="email"
                  type="email"
                  label="email"
                  autoComplete="email"
                  autoFocus
                  required
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  type="password"
                  label="password"
                  id="password"
                  autoComplete="current-password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <FormControlLabel
                        control={
                          <Checkbox
                            checked={checked}
                            onChange={(e) => setChecked(e.target.checked)}
                            color="primary"
                          />
                        }
                        label="Remember Me"
                      />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  className={classes.submit}
                >
                  Sign In
                </Button>
                <Grid container>
                  <Grid item xs>
                    <GoogleLogin
                        clientId={process.env.NEXT_PUBLIC_GMAIL_ID}
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="button-width text-center d-flex justify-content-center mb-3"
                    />
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs>
                    <Link href="#" variant="body2">
                      Forgot password?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="#" variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>

              </form>
            </div>
          </Container>
        </React.Fragment> 
        :
          window.location.replace('/records') 
      }
    </React.Fragment> 
  ) 
}

