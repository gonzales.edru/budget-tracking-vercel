import Head from 'next/head'
import { useState } from 'react';
import Router from 'next/router'
import Container from '@material-ui/core/Container';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Swal from 'sweetalert2'

export default function Register() {
	const [firstName , setFirstName] = useState('')
	const [lastName , setLastName] = useState('')
	const [email , setEmail] = useState('')
	const [mobile , setMobile] = useState('')
	const [password , setPassword] = useState('')
	const [password2 , setPassword2] = useState('')
	function register(e){
		e.preventDefault()
		if(password !== password2)
		{
			Swal.fire('Registration Failed', 'Password and confirm password does not match.', 'error')
		}
		else if(mobile.length < 11)
		{
			Swal.fire('Registration Failed', 'Mobile No. is not valid.', 'error')
		}
		else if(password.trim() == "" || password2.trim() == "" || firstName.trim() == "" || lastName.trim() == "")
		{
			Swal.fire('Registration Failed', 'Please fill out the required fields.', 'error')
		}
		else
		{
			const options = {
			    method: 'post',
			    headers: { 'Content-Type': 'application/json' },
			    body: JSON.stringify(
			    	{ 
			    		firstName: firstName,
			    		lastName: lastName,
			    		email: email,
			    		mobile: mobile,
			    		password: password
			    	}
			    )
			}
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/register`, options).then(res => res.json()).then(data => {
			    if (data.message === "Email already in use") {
			    	alert(data.message)
			    }
			    else if(data !== true)
			    {
			    	alert(data.err)
			    }
			    else
			    {
			    	Swal.fire('Registration Successful', 'Succesfully registered.', 'success')
			    	Router.push('/accounts/login')
			    }
			})
		}
		
	}

	const useStyles = makeStyles((theme) => ({
	  paper: {
	    marginTop: theme.spacing(10),
	    marginBottom: theme.spacing(17.3),
	    display: 'flex',
	    flexDirection: 'column',
	    alignItems: 'center',
	  },
	  avatar: {
	    margin: theme.spacing(1),
	    backgroundColor: theme.palette.secondary.main,
	  },
	  form: {
	    width: '100%', // Fix IE 11 issue.
	    marginTop: theme.spacing(1),
	  },
	  submit: {
	    margin: theme.spacing(1, 0, 2),
	  },
	}));

	const classes = useStyles();

	return(
		<React.Fragment>
			<Head>
			  <title>Register</title>
			  <link rel="icon" href="/favicon.ico" />
			</Head>
			<Container component="main" maxWidth="xs">
			            <CssBaseline />
			            <div className={classes.paper}>
			              <Avatar className={classes.avatar}>
			                <HowToRegIcon />
			              </Avatar>
			              <Typography component="h1" variant="h5" className="text-center">
			                Register
			              </Typography>
			              <form className={classes.form} onSubmit={(e) => {register(e)}} validate>
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  fullWidth
			                  type="email"
			                  label="Email"
			                  autoFocus
			                  required
			                  value={email}
			                  onChange={(e) => setEmail(e.target.value)}
			                />
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  fullWidth
			                  type="text"
			                  label="First Name"
			                  autoFocus
			                  required
			                  value={firstName}
			                  onChange={(e) => setFirstName(e.target.value)}
			                />
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  fullWidth
			                  type="text"
			                  label="Last name"
			                  autoFocus
			                  required
			                  value={lastName}
			                  onChange={(e) => setLastName(e.target.value)}
			                />
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  fullWidth
			                  type="number"
			                  label="Mobile"
			                  autoFocus
			                  required
			                  value={mobile}
			                  onChange={(e) => setMobile(e.target.value)}
			                />
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  required
			                  fullWidth
			                  type="password"
			                  label="password"
			                  id="password"
			                  autoComplete="current-password"
			                  value={password}
			                  onChange={(e) => setPassword(e.target.value)}
			                />
			                <TextField
			                  variant="outlined"
			                  margin="normal"
			                  required
			                  fullWidth
			                  type="password"
			                  label="Confirm Password"
			                  id="password"
			                  autoComplete="current-password"
			                  value={password2}
			                  onChange={(e) => setPassword2(e.target.value)}
			                />
			                <Button
			                  type="submit"
			                  fullWidth
			                  variant="contained"
			                  className={classes.submit}
			                >
			                  Register
			                </Button>
			              </form>
			            </div>
			          </Container>
		</React.Fragment>
	)
}