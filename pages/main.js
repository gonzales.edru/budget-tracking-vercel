import Head from 'next/head'
import { useState, useContext, useEffect } from 'react';
import { Button, Form, Col, Table, Row, Container, Modal } from 'react-bootstrap'
import UserContext from '../UserContext';
import styles from '../styles/Home.module.css'
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

export default function Main() {

	const { user } = useContext(UserContext)
	const [name, setName] = useState('')
	const [data, setData] = useState([])
	const [search, setSearch] = useState('')
	const [sortCategory, setSortCategory] = useState('0')
	const [color, setColor] = useState('')
	const [category, setCategory] = useState(0)
	const [action, setAction] = useState(0)
	const [id, setId] = useState('')
	const [show, setShow] = useState(false);
	const [isActive, setIsActive] = useState(false);
	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(5);
	  const useStyles1 = makeStyles((theme) => ({
	    root: {
	      flexShrink: 0,
	      marginLeft: theme.spacing(2.5),
	      height: 50,

	    },
	  }));

		function TablePaginationActions(props) {
		    const classes = useStyles1();
		    const theme = useTheme();
		    const { count, page, rowsPerPage, onChangePage } = props;

		    const handleFirstPageButtonClick = (event) => {
		      onChangePage(event, 0);
		    };

		    const handleBackButtonClick = (event) => {
		      onChangePage(event, page - 1);
		    };

		    const handleNextButtonClick = (event) => {
		      onChangePage(event, page + 1);
		    };

		    const handleLastPageButtonClick = (event) => {
		      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
		    };

		    return (
		      <div className={classes.root}>
		        <IconButton
		          onClick={handleFirstPageButtonClick}
		          disabled={page === 0}
		          aria-label="first page"
		        >
		          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
		        </IconButton>
		        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
		          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
		        </IconButton>
		        <IconButton
		          onClick={handleNextButtonClick}
		          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
		          aria-label="next page"

		        >
		          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
		        </IconButton>
		        <IconButton
		          onClick={handleLastPageButtonClick}
		          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
		          aria-label="last page"

		        >
		          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
		        </IconButton>
		      </div>
		    );
	  }
	  
	  const useStyles2 = makeStyles((theme) => ({
	  	table: {
	    minWidth: 300,
	  	},
	  }));

	  const classes = useStyles2();
	  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

	  const handleChangePage = (event, newPage) => {
	    setPage(newPage);
	  };

	  const handleChangeRowsPerPage = (event) => {
	    setRowsPerPage(parseInt(event.target.value, 10));
	    setPage(0);
	  };
	const handleShow = (action, categoryId) => {
		setAction(action)
		setId(categoryId)
		if(action == 2){
			const accessToken  = localStorage.getItem('token')
			const options = {
			    method: 'get',
			    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`, id: categoryId }
			}
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/getCategoryById`, options).then(res => res.json()).then(data => {
			   setName(data.name)
			   setCategory(data.category)
			   setIsActive(false)
			})
		}
		else if(action == 3){			
			const accessToken  = localStorage.getItem('token')
			const options = {
			    method: 'get',
			    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }`, id: categoryId }
			}
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/getCategoryById`, options).then(res => res.json()).then(data => {
			   if(data !== null){
			   		setName(data.name)
			   		setCategory(data.category)
			   		setIsActive(true)
			   }
			   else
			   {
			   		alert("Something went wrong please try again later")
			   }
			})
		}
		else
		{
			setIsActive(false)
		}
		setShow(true);
	}
	const handleClose = () => {
		setName('')
		setCategory(0)
		setShow(false)
	}

	useEffect(() => {
		const accessToken  = localStorage.getItem('token')
		const options = {
		    method: 'get',
		    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }` }
		}
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/categories`, options).then(res => res.json()).then(data => {
		    if(data !== null)
		    {
		    	if (sortCategory !== '0') {
		    		const filterByCategory = data.filter(data => data.category == sortCategory)
		    		setData(filterByCategory)
		    	}
		    	else if (search !== '') {

		    		const filterBySearch = data.filter(data => data.name.toLowerCase().includes(search.toLowerCase()))
		    		setData(filterBySearch)
		    	}
		    	else
		    	{
		    		setData(data)
		    	}
		    }
		    else
		    {
		    	alert("Something went wrong please try again later")
		    }
		})
	}, [sortCategory, search, name])

	function enroll(e){
		e.preventDefault();
		if(name == "" || category == ""){
			alert("Please fill up the required fields")
		}
		else
		{
			if (action == 1) {
				const accessToken  = localStorage.getItem('token')
				const options = {
				    method: 'post',
				    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }` },
				    body: JSON.stringify(
				    	{ 
				    		name: name,
				    		category: category,
				    		userId: user.id
				    	}
				    )
				}
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/enrollCategory`, options).then(res => res.json()).then(data => {
				    if(data !== '')
				    {
				    	handleClose()
				    }
				    else
				    {
				    	alert("Something went wrong please try again later")
				    }
				})
			}
			else if(action == 2){
				const accessToken  = localStorage.getItem('token')
				const options = {
				    method: 'post',
				    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }` },
				    body: JSON.stringify(
				    	{ 
				    		id: id,
				    		name: name,
				    		category: category,
				    		isArchived: false
				    	}
				    )
				}
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/categoryUpdate`, options).then(res => res.json()).then(data => {
				   	handleClose()
				   	setName('')
				   	setCategory(0)
				})	
			}
			else
			{
				const accessToken  = localStorage.getItem('token')
				const options = {
				    method: 'post',
				    headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ accessToken }` },
				    body: JSON.stringify(
				    	{ 
				    		id: id,
				    		name: name,
				    		category: category,
				    		isArchived: true
				    	}
				    )
				}
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/categories/categoryUpdate`, options).then(res => res.json()).then(data => {
				   	handleClose()
				   	setName('')
				   	setCategory(0)
				})	
			}
			
		} 
	}

	return (
		<React.Fragment>
	   		<Head>
	   		  <title>Categories</title>
	   		  <link rel="icon" href="/favicon.ico" />
	   		</Head>
	   		{ user.id !== undefined
	   			? 
	   			<React.Fragment>
		   			<Container className="mt-5">
			   			<Row>
			   				<Col sm={12}>
				        		<h4 className="text-center">Categories</h4>
					    	</Col>
					    </Row>
					    <Row>			
			    	    	<Col sm={4} lg={4}>
			    	    	  <Button className="record-button-width text-center mb-2 bg-dark" onClick = {(e) => handleShow(1)}>
			    	    	    Add record
			    	    	  </Button>
			    	    	</Col>
			    	       	<Col sm={4}>
			    		        <Form.Control
			    			        className="mb-2"
			    			        id="inlineFormInput"
			    			        placeholder="Search"
			    			        value = {search} 
			    			        onChange = {(e) => setSearch(e.target.value)}
			    		        />
			    	        </Col>
			    	        <Col sm={4}>
			    		       	<Form.Control as="select" className="mb-2" defaultValue={sortCategory} onChange = {(e) => setSortCategory(e.target.value)} required>
			    			       	    <option value="0" >Category</option>
			    			       	    <option value="income">Income</option>
			    			       	    <option value="expense">Expense</option>
			    		       	</Form.Control>
			    	       	</Col>
					    </Row>
					    <Row>
						    <Col sm={12} className="justify-content-center">
						    	<Table striped bordered hover variant="dark">
						    		<thead className="text-center">
							    	    <tr>
							    	      	<th>Name</th>	   
								    	    <th>Category</th>
								    	    <th>Action</th>
							    	    </tr>
						    	  	</thead>
						    		<tbody className="text-center">
							    	    {(rowsPerPage > 0
			    	    		            ? data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
			    	    		            : data
			    	    	          		).map((data) => (
			    	    			    	    <tr key={data._id} className={color}>
			    	    			    	      	<td>{data.name}</td>
			    	    			    	      	<td>{data.category}</td>
			    	    			    	      	<td>
			    	    				    	      	<Button className="table-button-width text-center bg-dark mr-1" onClick = {(e) => handleShow(2, data._id)}>
			    	    			    	    			Edit
			    	    			    	  			</Button>
			    	    				    	      	<Button className="table-button-width text-center bg-danger" onClick = {(e) => handleShow(3, data._id)}>
			    	    			    	    			Delete
			    	    			    	  			</Button>
			    	    				  			</td>
			    	    			    	    </tr>
											)
			    	    	          	)}
						    		</tbody>
				    		        <TableFooter>
				    		          	<TableRow>
				    			            <TablePagination
				    			              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
				    			              colSpan={3}
				    			              count={data.length}
				    			              rowsPerPage={rowsPerPage}
				    			              page={page}
				    			              SelectProps={{
				    			                inputProps: { 'aria-label': 'rows per page' },
				    			                native: true,
				    			              }}
				    			              onChangePage={handleChangePage}
				    			              onChangeRowsPerPage={handleChangeRowsPerPage}
				    			              ActionsComponent={TablePaginationActions}
				    			              className="bg-light"
				    			            />
				    		         	</TableRow>
				    		        </TableFooter>
						   		</Table>
						   	</Col>
					    </Row>
					</Container>
		    	    <Modal
		    	    	show={show} onHide={handleClose}
		    	    	size="lg"
		    	    	key={data._id}
		    	    	aria-labelledby="contained-modal-title-vcenter"
		    	    	centered
		    	    	>
		    	    	<Modal.Header closeButton>
		    	    	{action == 1 
		    	    	? 
		    	    		<Modal.Title id="contained-modal-title-vcenter">
		    	          		New Record
		    	        	</Modal.Title>
		    	    	: (action == 2 
		    	    		?
		    	    			<React.Fragment>
			    	    			<Form.Control type="hidden" value={id} required />
	    	    					<Modal.Title id="contained-modal-title-vcenter">
	    	    			      		Edit Record
	    	    			    	</Modal.Title>
    	    			    	</React.Fragment>
		    	    		:
		    	    			<React.Fragment>
			    	    			<Form.Control type="hidden" value={id} required />
		    	    				<Modal.Title id="contained-modal-title-vcenter">
		    	    			  		Delete Record
		    	    				</Modal.Title>
	    	    				</React.Fragment>
		    	    		)        		
		    	    	}	
		    	      	</Modal.Header>
		    	      	<Modal.Body>
		      		        <Form.Group as={Row} controlId="formPlaintextEmail">
		      		        	
        	                  	<Form.Label column sm="3">
        		                    Name
        		                </Form.Label>
        		                <Col sm="9">
        		                    <Form.Control type="text" className="mb-2" placeholder="Name" value={name} onChange={(e) => setName(e.target.value)} disabled={isActive} required />
        		                </Col>
        	                </Form.Group>
              		        <Form.Group as={Row} controlId="formPlaintextEmail">
	    	                  	<Form.Label column sm="3">
	    		                    Category
	    		                </Form.Label>
	    		                <Col sm="9">
	    		                    <Form.Control as="select" className="mb-2" value={category} onChange={(e) => setCategory(e.target.value)} disabled={isActive} required>
	    					       	    <option value='0' disabled>Choose...</option>
	    					       	    <option value="income">Income</option>
	    					       	    <option value="expense">Expense</option>
	    				       		</Form.Control>
	    		                </Col>
	    	                </Form.Group>
		    	      	</Modal.Body>
		    	      	<Modal.Footer className="d-flex justify-content-center">
		    	      		<Button variant="primary" className="modal-button-width bg-dark" onClick={enroll}>Submit</Button>
		    	      		<Button className="modal-button-width bg-dark" onClick={handleClose}>Close</Button>
		    	      	</Modal.Footer>
		    	    </Modal>
				</React.Fragment> 
			    :
			    	window.location.replace('/accounts/login')
			    } 
	  	</React.Fragment>
	 )
}
