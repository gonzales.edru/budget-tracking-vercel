import { useContext } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar(){

	const { user } = useContext(UserContext);
	return(
		<Navbar collapseOnSelect expand="lg" className="color-nav" variant="dark">
		  	<Navbar.Brand href="/">Budget Tracker</Navbar.Brand>
		  	<Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  	<Navbar.Collapse id="responsive-navbar-nav">
			    <Nav className="mr-auto">
			    { user.id !== undefined || null 
			    	?
				    	<React.Fragment>
				    		<Nav.Link href="/main">Categories</Nav.Link>
				    		<Nav.Link href="/records">Records</Nav.Link>
				    		<Nav.Link href="/overview">Overview</Nav.Link>
				    	</React.Fragment> 
			    	:
			    		null
			   	}
			     
			    </Nav>
		    <Nav>
		      	
		      	{ user.id !== undefined || null 
		      		?

		      		<Nav.Link eventKey={2} href="/accounts/logout">
			       		Log out
			      	</Nav.Link>
		      		
		      		:
		      		<React.Fragment>
			      		<Nav.Link eventKey={2} href="/accounts/login">
				       		Log in
				      	</Nav.Link>
				      	<Nav.Link href="/accounts/register">Register</Nav.Link>
				    </React.Fragment>
		      	}
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}

