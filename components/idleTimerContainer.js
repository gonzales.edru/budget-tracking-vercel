import { useRef, useContext } from 'react'
import IdleTimer from 'react-idle-timer'
import UserContext from '../UserContext';
import Router from 'next/router'

export default function Idle() {
	const idleTimerRef = useRef(null)
	const { unsetUser, setUser } = useContext(UserContext)

	const onIdle = () =>{
		unsetUser();
		Router.push('/accounts/login')
	}

	return(
		<div>
			<IdleTimer ref = {idleTimerRef} timeout = { 900000 } onIdle={onIdle}></IdleTimer>
		</div>
	)
}